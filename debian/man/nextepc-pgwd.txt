NAME
  nextepc-pgwd - Packet Data Network Gateway (EPC / LTE)

SYNOPSIS
  nextepc-pgwd [options]

DESCRIPTION

  nextepc-pgwd is the daemon for the Packet Data Network Gateway (PGWD) as part
  of nextepc.

  The PGWD is the gateway between the EPC and the external
  packet data network, such as the public Internet. It implements the S5
  interface towards the S-GW, the SGi interface towards the Internet,
  and the S7 interface towards the PCRF.


OPTIONS

  -v    Show version
  -h    Show help
  -d    Start as daemon
  -f <conf>   Set configuration file name.
  -l <log_file>   Log file path to be logged to
  -p <pid_file>   PID file path

 
AUTHOR

  This manual page was written by Ruben Undheim <ruben.undheim@gmail.com>
  for the Debian project (and may be used by others).
