NAME
  nextepc-mmed - Mobile Management Entity (EPC / LTE)

SYNOPSIS
  nextepc-mmed [options]

DESCRIPTION
  nextepc-mmed is the Mobile Management Entity (MME) for nextepc.

  MME terminates the S1 interfaces from the eNodeBs cells in the cellular
  network, and interfaces via S11 to the SGW as well as via S6a to the
  HSS.

OPTIONS

  -v    Show version
  -h    Show help
  -d    Start as daemon
  -f <conf>   Set configuration file name.
  -l <log_file>   Log file path to be logged to
  -p <pid_file>   PID file path

 
AUTHOR

  This manual page was written by Ruben Undheim <ruben.undheim@gmail.com>
  for the Debian project (and may be used by others).
