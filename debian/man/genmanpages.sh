#!/bin/bash

txt2man -d "${CHANGELOG_DATE}" -t NEXTEPC-HSSD   -s 8 nextepc-hssd.txt  > nextepc-hssd.8  
txt2man -d "${CHANGELOG_DATE}" -t NEXTEPC-MMED   -s 8 nextepc-mmed.txt  > nextepc-mmed.8
txt2man -d "${CHANGELOG_DATE}" -t NEXTEPC-PCRFD  -s 8 nextepc-pcrfd.txt > nextepc-pcrfd.8
txt2man -d "${CHANGELOG_DATE}" -t NEXTEPC-PGWD   -s 8 nextepc-pgwd.txt  > nextepc-pgwd.8
txt2man -d "${CHANGELOG_DATE}" -t NEXTEPC-SGWD   -s 8 nextepc-sgwd.txt  > nextepc-sgwd.8     

